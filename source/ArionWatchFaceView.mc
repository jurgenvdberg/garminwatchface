import Toybox.Graphics;
import Toybox.Application;
import Toybox.Lang;
import Toybox.Time;
import Toybox.Time.Gregorian;
import Toybox.System;
import Toybox.WatchUi;

class ArionWatchFaceView extends WatchUi.WatchFace {

    private var im_logo;
    var im_logo_xorg, im_logo_yorg;

    function initialize() {
        WatchFace.initialize();       
    }

    // Load your resources here
    function onLayout(dc as Dc) as Void {
        setLayout(Rez.Layouts.WatchFace(dc));

        im_logo = Application.loadResource(Rez.Drawables.im_arionLogo);
        im_logo_xorg = dc.getWidth()/2 - im_logo.getWidth()/2;
        im_logo_yorg = 0 + (160 - im_logo.getHeight())/2;
    }

    // Called when this View is brought to the foreground. Restore
    // the state of this View and prepare it to be shown. This includes
    // loading resources into memory.
    function onShow() as Void {
    }

    function getDayString() {
        // Get and show the current day
        var now = new Time.Moment(Time.today().value());
        var timeInfo = Gregorian.utcInfo(now, Time.FORMAT_SHORT );
        var dayString;

        // Note: there seems to be a bug in the SDK where the day_of_week is not returned
        // corrently for Sundays. This little hack solves that.
        switch ((timeInfo.day_of_week + 1) % 7){
            case 1:     dayString = "Sunday";       break;
            case 2:     dayString = "Monday";       break;
            case 3:     dayString = "Tuesday";      break;
            case 4:     dayString = "Wednesday";    break;
            case 5:     dayString = "Thursday";     break;
            case 6:     dayString = "Friday";       break;
            case 7:     dayString = "Saturday";     break;
            default:    dayString = "Today";
        }

        return dayString;
    }

    // Update the view
    function onUpdate(dc as Dc) as Void {
        // Get and show today as string
        var dayString = getDayString();
        var dayView = View.findDrawableById("DayLabel") as Text;
        dayView.setText(dayString);


        // Get and show the current time
        var clockTime = System.getClockTime();
        var timeString = Lang.format("$1$:$2$", [clockTime.hour, clockTime.min.format("%02d")]);
        var view = View.findDrawableById("TimeLabel") as Text;
        view.setText(timeString);

        // Call the parent onUpdate function to redraw the layout
        View.onUpdate(dc);

        dc.drawBitmap(im_logo_xorg, im_logo_yorg, im_logo);
    }

    // Called when this View is removed from the screen. Save the
    // state of this View here. This includes freeing resources from
    // memory.
    function onHide() as Void {
    }

    // The user has just looked at their watch. Timers and animations may be started here.
    function onExitSleep() as Void {
    }

    // Terminate any active timers and prepare for slow updates.
    function onEnterSleep() as Void {
    }

}
